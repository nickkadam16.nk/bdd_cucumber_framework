Feature: Trigger all API's 

Scenario: Trigger the post API and validate the response 
	Given post prerequisites 
	When post request 
	Then post status code 
	And post response body
	
Scenario: Trigger the put API and validate the response
	Given put prerequisites 
	When put request 
	Then put status code 
	And put response body