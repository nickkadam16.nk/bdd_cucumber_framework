Feature: Trigger Post and Put API's with Data Driven 

@postDataDriven
Scenario Outline: Trigger the Post API request with valid request params and validate response body 
	Given Post Enter "<Name>" and "<Job>" in request body 
	When Post Send the data to request with Payload 
	Then Post Validate data and status code 
	And Post Validate data and response body parameters 
	
Examples:
	|Name|Job|
	|Nikhil|QA|
	|Sanket|Admin|
	|Darshan|CA|
	
@putDataDriven
Scenario Outline: Trigger the Put API request with valid request params and validate response body
	Given Put Enter "<Name>" and "<Job>" in request body 
	When Put Send the data to request with Payload 
	Then Put Validate data and status code 
	And Put Validate data and response body parameters 
	
Examples:
	|Name|Job|
	|Apurva|QA|
	|Mayuri|Admin|
	|Shruti|Manager|