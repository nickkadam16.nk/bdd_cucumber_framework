Feature: Trigger All API's 

@post
Scenario: Trigger the Post API request with valid request params and validate response body 
	Given Post Enter NAME and JOB in request body 
	When Post Send the request with Payload 
	Then Post Validate status code 
	And Post Validate response body parameters 
	
@put
Scenario: Trigger the Put API request with valid request params and validate response body 
	Given Put Enter NAME and JOB in request body 
	When Put Send the request with Payload 
	Then Put Validate status code 
	And Put Validate response body parameters 
	
@get1
Scenario: Trigger the Get1 API request and validate response body 
	Given Get1 Enter endpoint 
	When Get1 Send the request 
	Then Get1 Validate status code 
	And Get1 Validate response body parameters 
	
@get2
Scenario: Trigger the Get2 API request and validate response body 
	Given Get2 Enter endpoint 
	When Get2 Send the request 
	Then Get2 Validate status code 
	And Get2 Validate response body parameters 
	
	
	
