package step_Definitions;

import static io.restassured.RestAssured.given;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import endpoints.Endpoints;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import request_Repo.Request_Repo;
import utility_Common_Methods.Handle_API_logs;
import utility_Common_Methods.Handle_dir;

public class Post_Step_Definition {

	File log_dir;
	String request_body;
	String endpoint;
	int status_code;
	String response_body;

	@Given("Post Enter NAME and JOB in request body")
	public void post_enter_name_and_job_in_request_body() throws IOException {
		log_dir = Handle_dir.create_log_dir("Post_Dir");
		endpoint = Endpoints.post_endpoint();
		request_body = Request_Repo.post_request_repo();
//		throw new io.cucumber.java.PendingException();
	}

	@When("Post Send the request with Payload")
	public void post_send_the_request_with_payload() {
		status_code = given().header("Content-Type", "application/json").body(request_body).when().post(endpoint).then()
				.extract().response().statusCode();
		System.out.println("Post API statusCode : " + status_code);
		response_body = given().header("Content-Type", "application/json").body(request_body).when().post(endpoint).then()
				.extract().response().asString();
//		throw new io.cucumber.java.PendingException();
	}

	@Then("Post Validate status code")
	public void post_validate_status_code() {
		Assert.assertEquals(status_code, 201);
//		throw new io.cucumber.java.PendingException();
	}

	@Then("Post Validate response body parameters")
	public void post_validate_response_body_parameters() throws IOException {
		JsonPath JPReq = new JsonPath(request_body);
		String reqName = JPReq.getString("name");
		String reqJob = JPReq.getString("job");
		LocalDateTime curr_date = LocalDateTime.now();
		String exp_date = curr_date.toString().substring(0, 11);

		JsonPath JPRes = new JsonPath(response_body);
		int resId = JPRes.getInt("id");
		String resName = JPRes.getString("name");
		String resJob = JPRes.getString("job");
		String resDate = JPRes.getString("createdAt").substring(0, 11);

		Assert.assertNotNull(resId);
		Assert.assertEquals(reqName, resName);
		Assert.assertEquals(reqJob, resJob);
		Assert.assertEquals(exp_date, resDate);

		String Post_Log_File = this.getClass().getName();
		Handle_API_logs.evidence_creator(log_dir, Post_Log_File, request_body, endpoint, response_body);
		
//		throw new io.cucumber.java.PendingException();
	}

}
