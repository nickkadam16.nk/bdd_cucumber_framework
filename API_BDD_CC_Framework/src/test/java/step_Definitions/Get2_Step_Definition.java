package step_Definitions;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;

import endpoints.Endpoints;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import utility_Common_Methods.Handle_API_logs;
import utility_Common_Methods.Handle_dir;

public class Get2_Step_Definition {

	File log_dir;
	String request_body;
	String endpoint;
	int status_code;
	String response_body;
	
	@Given("Get2 Enter endpoint")
	public void get2_enter_endpoint() {
		log_dir = Handle_dir.create_log_dir("Get2_Dir");
		request_body = null;
		endpoint = Endpoints.get2_endpoint();
	}

	@When("Get2 Send the request")
	public void get2_send_the_request() {
		status_code = given().header("Content-Type", "application/json").when().get(endpoint).then().extract().response()
				.statusCode();
		System.out.println("Get2 API statusCode : " + status_code);
		response_body = given().header("Content-Type", "application/json").when().get(endpoint).then().extract()
				.response().asString();
	}

	@Then("Get2 Validate status code")
	public void get2_validate_status_code() {
		Assert.assertEquals(status_code, 200);
	}

	@Then("Get2 Validate response body parameters")
	public void get2_validate_response_body_parameters() throws IOException {
		int exp_id[] = { 7, 8, 9, 10, 11, 12 };
		String exp_email[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		String exp_fName[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String exp_lName[] = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };

		JSONObject JO = new JSONObject(response_body);
		JSONArray JArr = JO.getJSONArray("data");
		int count = JArr.length();

		for (int i = 0; i < count; i++) {
			int res_id = JArr.getJSONObject(i).getInt("id");
			String res_email = JArr.getJSONObject(i).getString("email");
			String res_fName = JArr.getJSONObject(i).getString("first_name");
			String res_lName = JArr.getJSONObject(i).getString("last_name");

			Assert.assertEquals(exp_id[i], res_id);
			Assert.assertEquals(exp_email[i], res_email);
			Assert.assertEquals(exp_fName[i], res_fName);
			Assert.assertEquals(exp_lName[i], res_lName);
		}
		String Get2_Log_File = this.getClass().getName();
		Handle_API_logs.evidence_creator(log_dir, Get2_Log_File, request_body, endpoint, response_body);
	}

}
