package step_Definitions;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import endpoints.Endpoints;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import request_Repo.DD_Request_Repo;
import utility_Common_Methods.Handle_API_logs;
import utility_Common_Methods.Handle_dir;

public class DD_Post_Step_Definition {

	@Before
	public void set_up() {
		log_dir = Handle_dir.create_log_dir("Post_Dir");
		System.out.println("with before hook log dir created: " + log_dir);
	}

	@After
	public void tear_down() throws IOException {
		String Post_Log_File = this.getClass().getName();
		System.out.println("with after hook log file created: " + Post_Log_File);
		Handle_API_logs.evidence_creator(log_dir, Post_Log_File, request_body, endpoint, response_body);
	}

	File log_dir;
	String request_body;
	String endpoint;
	int status_code;
	String response_body;

	@Given("Post Enter {string} and {string} in request body")
	public void post_enter_and_in_request_body(String reqName, String reqJob) {
		endpoint = Endpoints.post_endpoint();
		request_body = DD_Request_Repo.post_request_repo(reqName, reqJob);
//		throw new io.cucumber.java.PendingException();
	}

	@When("Post Send the data to request with Payload")
	public void post_send_the_data_to_request_with_payload() {
		status_code = given().header("Content-Type", "application/json").body(request_body).when().post(endpoint).then()
				.extract().response().statusCode();
		System.out.println("Post API statusCode : " + status_code);
		response_body = given().header("Content-Type", "application/json").body(request_body).when().post(endpoint)
				.then().extract().response().asString();
		System.out.println("Post API responseBody : " + response_body);
//		throw new io.cucumber.java.PendingException();
	}

	@Then("Post Validate data and status code")
	public void post_validate_data_and_status_code() {
		Assert.assertEquals(status_code, 201);
//		throw new io.cucumber.java.PendingException();
	}

	@Then("Post Validate data and response body parameters")
	public void post_validate_data_and_response_body_parameters() throws IOException {
		JsonPath JPReq = new JsonPath(request_body);
		String reqName = JPReq.getString("name");
		String reqJob = JPReq.getString("job");
		LocalDateTime curr_date = LocalDateTime.now();
		String exp_date = curr_date.toString().substring(0, 11);

		JsonPath JPRes = new JsonPath(response_body);
		int resId = JPRes.getInt("id");
		String resName = JPRes.getString("name");
		String resJob = JPRes.getString("job");
		String resDate = JPRes.getString("createdAt").substring(0, 11);

		Assert.assertNotNull(resId);
		Assert.assertEquals(reqName, resName);
		Assert.assertEquals(reqJob, resJob);
		Assert.assertEquals(exp_date, resDate);

//		throw new io.cucumber.java.PendingException();
	}

}
