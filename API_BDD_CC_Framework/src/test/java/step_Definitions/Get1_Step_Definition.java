package step_Definitions;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;

import endpoints.Endpoints;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import utility_Common_Methods.Handle_API_logs;
import utility_Common_Methods.Handle_dir;

public class Get1_Step_Definition {

	File log_dir;
	String request_body;
	String endpoint;
	int status_code;
	String response_body;

	@Given("Get1 Enter endpoint")
	public void get1_enter_endpoint() {
		log_dir = Handle_dir.create_log_dir("Get1_Dir");
		request_body = null;
		endpoint = Endpoints.get1_endpoint();
	}

	@When("Get1 Send the request")
	public void get1_send_the_request() {
		status_code = given().header("Content-Type", "application/json").when().get(endpoint).then().extract().response()
				.statusCode();
		System.out.println("Get1 API statusCode : " + status_code);
		response_body = given().header("Content-Type", "application/json").when().get(endpoint).then().extract()
				.response().asString();
	}

	@Then("Get1 Validate status code")
	public void get1_validate_status_code() {
		Assert.assertEquals(status_code, 200);
	}

	@Then("Get1 Validate response body parameters")
	public void get1_validate_response_body_parameters() throws IOException {
		int exp_id[] = { 1, 2, 3, 4, 5, 6 };
		String exp_email[] = { "george.bluth@reqres.in", "janet.weaver@reqres.in", "emma.wong@reqres.in",
				"eve.holt@reqres.in", "charles.morris@reqres.in", "tracey.ramos@reqres.in" };
		String exp_fName[] = { "George", "Janet", "Emma", "Eve", "Charles", "Tracey" };
		String exp_lName[] = { "Bluth", "Weaver", "Wong", "Holt", "Morris", "Ramos" };

		JSONObject JO = new JSONObject(response_body);
		JSONArray JArr = JO.getJSONArray("data");
		int count = JArr.length();

		for (int i = 0; i < count; i++) {
			int res_id = JArr.getJSONObject(i).getInt("id");
			String res_email = JArr.getJSONObject(i).getString("email");
			String res_fName = JArr.getJSONObject(i).getString("first_name");
			String res_lName = JArr.getJSONObject(i).getString("last_name");

			Assert.assertEquals(exp_id[i], res_id);
			Assert.assertEquals(exp_email[i], res_email);
			Assert.assertEquals(exp_fName[i], res_fName);
			Assert.assertEquals(exp_lName[i], res_lName);
		}
		String Get1_log_file = this.getClass().getName();
		Handle_API_logs.evidence_creator(log_dir, Get1_log_file, request_body, endpoint, response_body);
	}

}
