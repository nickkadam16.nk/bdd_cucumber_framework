package utility_Common_Methods;

import java.io.File;

public class Handle_dir {

	public static File create_log_dir(String log_dir) {
		String project_dir = System.getProperty("user.dir");

		File directory = new File(project_dir + "\\BDD_CC_Log_Files\\" + log_dir);

		if (directory.exists()) {
			directory.delete();
			directory.mkdir();
		} else {
			directory.mkdir();
		}
		return directory;
	}
}
