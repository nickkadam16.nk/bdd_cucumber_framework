package utility_Common_Methods;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel_data_extractor {

	public static ArrayList<String> excel_data_reader(String fileName, String sheetName, String tcName)
			throws IOException {
		ArrayList<String> arr_data = new ArrayList<String>();

		String project_dir = System.getProperty("user.dir");

		FileInputStream fis = new FileInputStream(project_dir + "\\Data_File\\" + fileName + ".xlsx");

		XSSFWorkbook wb = new XSSFWorkbook(fis);

		int count = wb.getNumberOfSheets();

		for (int i = 0; i < count; i++) {
			String sheet_name = wb.getSheetName(i);
			if (sheet_name.equals(sheetName)) {
				XSSFSheet sheet = wb.getSheetAt(i);
				Iterator<Row> row = sheet.iterator();
				row.next();
				while (row.hasNext()) {
					Row data_row = row.next();
					String tc_name = data_row.getCell(0).getStringCellValue();
					if (tc_name.equals(tcName)) {
						Iterator<Cell> cell_values = data_row.iterator();
						while (cell_values.hasNext()) {
							String test_data = cell_values.next().getStringCellValue();
							arr_data.add(test_data);
						}
						break;
					}

				}
			}
		}
		wb.close();
		return arr_data;

	}
}
