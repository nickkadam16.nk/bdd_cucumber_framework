//package utility_Common_Methods;
//
//import org.testng.ITestContext;
//import org.testng.ITestResult;
//import com.aventstack.extentreports.ExtentReports;
//import com.aventstack.extentreports.ExtentTest;
//import com.aventstack.extentreports.Status;
//import com.aventstack.extentreports.markuputils.ExtentColor;
//import com.aventstack.extentreports.markuputils.MarkupHelper;
//import com.aventstack.extentreports.reporter.ExtentSparkReporter;
//import com.aventstack.extentreports.reporter.configuration.Theme;
//
//public class Extent_report {
//
//	ExtentSparkReporter sparkReporter;
//	ExtentReports extentReport;
//	ExtentTest test;
//	
//	public void reportConfigurations() {
//		sparkReporter = new ExtentSparkReporter(".\\extent_report\\.report.html");
//		extentReport = new ExtentReports();
//		
//		extentReport.attachReporter(sparkReporter);
//		
////		adding system env info to reports
//		extentReport.setSystemInfo("OS", "Windows 11");
//		extentReport.setSystemInfo("User", "Nikhil Kadam");
//		
////		configuration of changing look and feel of report
//		sparkReporter.config().setDocumentTitle("BDD-CC Extent Listener Report");
//		sparkReporter.config().setReportName("BDD-CC Framework Extent Report");
//		sparkReporter.config().setTheme(Theme.DARK);
//	}
//	
//	public void onStart(ITestContext result) {
//		reportConfigurations();
//		System.out.println("On Start Method Invoked...");
//	}
//	
//	public void onFinish(ITestContext result) {
//		System.out.println("On Finish Method Invoked...");
//		extentReport.flush();
//	}
//	
//	public void onTestFailure(ITestResult result) {
//		System.out.println("Name of the test method failed: "+result.getName());
//		test = extentReport.createTest(result.getName());
//		test.log(Status.FAIL, MarkupHelper.createLabel("Name of the failed test case is: "+result.getName(), ExtentColor.RED));
//	}
//	
//	public void onTestSkipped(ITestResult result) {
//		System.out.println("Name of the test method skipped: "+result.getName());
//		test = extentReport.createTest(result.getName());
//		test.log(Status.SKIP, MarkupHelper.createLabel("Name of the skipped test case is: "+result.getName(), ExtentColor.YELLOW));
//	}
//
//	public void onTestStart(ITestResult result) {
//		System.out.println("Name of the method started: "+result.getName());
//	}
//	
//	public void onTestSuccess(ITestResult result) {
//		System.out.println("Name of the test method executed successfully"+result.getName());
//		test = extentReport.createTest(result.getName());
//		test.log(Status.PASS, MarkupHelper.createLabel("Name of the successfull test case is: "+result.getName(), ExtentColor.GREEN));
//	}
//	
//	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
//		
//	}
//	
//}
