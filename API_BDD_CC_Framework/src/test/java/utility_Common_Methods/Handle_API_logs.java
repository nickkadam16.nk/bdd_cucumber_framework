package utility_Common_Methods;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Handle_API_logs {

	public static void evidence_creator(File logDirName, String fileName, String requestBody, String endpoint,
			String responseBody) throws IOException {
		File createFile = new File(logDirName + "\\" + fileName + ".txt");

		FileWriter dataWriter = new FileWriter(createFile);
		dataWriter.write("Request Body is: " + requestBody + "\n\n");
		dataWriter.write("End-Point is: " + endpoint + "\n\n");
		dataWriter.write("Response Body is: " + responseBody);
		dataWriter.close();

	}

}