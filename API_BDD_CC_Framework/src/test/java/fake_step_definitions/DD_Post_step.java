package fake_step_definitions;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import endpoints.Endpoints;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import utility_Common_Methods.Handle_API_logs;
import utility_Common_Methods.Handle_dir;

public class DD_Post_step {

	File log_dir;
	String endpoint;
	String request_body;
	int status_code;
	String response_body;

	@Given("prerequisites {string} and {string}")
	public void prerequisites_and(String name, String job) {
		log_dir = Handle_dir.create_log_dir("Post_Dir");
		endpoint = Endpoints.post_endpoint();
		request_body = "{\r\n" + "    \"name\": \"" + name + "\",\r\n" + "    \"job\": \"" + job + "\"\r\n" + "}";
//		throw new io.cucumber.java.PendingException();
	}

	@When("request")
	public void request() {
		status_code = given().header("Content-Type", "application/json").body(request_body).when().post(endpoint).then()
				.extract().response().statusCode();
		System.out.println("Post API statusCode : " + status_code);
		response_body = given().header("Content-Type", "application/json").body(request_body).when().post(endpoint)
				.then().extract().response().asString();
		System.out.println("Post API responseBody : " + response_body);
//		throw new io.cucumber.java.PendingException();
	}

	@Then("status code")
	public void status_code() {
		Assert.assertEquals(status_code, 201);
//		throw new io.cucumber.java.PendingException();
	}

	@Then("response body")
	public void response_body() throws IOException {
		JsonPath JPReq = new JsonPath(request_body);
		String reqName = JPReq.getString("name");
		String reqJob = JPReq.getString("job");
		LocalDateTime curr_date = LocalDateTime.now();
		String exp_date = curr_date.toString().substring(0, 11);

		JsonPath JPRes = new JsonPath(response_body);
		int resId = JPRes.getInt("id");
		String resName = JPRes.getString("name");
		String resJob = JPRes.getString("job");
		String resDate = JPRes.getString("createdAt").substring(0, 11);

		Assert.assertNotNull(resId);
		Assert.assertEquals(reqName, resName);
		Assert.assertEquals(reqJob, resJob);
		Assert.assertEquals(exp_date, resDate);

		String Post_Log_File = this.getClass().getName();
		Handle_API_logs.evidence_creator(log_dir, Post_Log_File, request_body, endpoint, response_body);
//		throw new io.cucumber.java.PendingException();
	}
}
