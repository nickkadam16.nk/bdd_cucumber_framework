package endpoints;

public class Endpoints {

	public static String post_endpoint() {
		String post_endpoint = "https://reqres.in/api/users";
		return post_endpoint;
	}

	public static String put_endpoint() {
		String put_endpoint = "https://reqres.in/api/users/2";
		return put_endpoint;
	}

	public static String get1_endpoint() {
		String get1_endpoint = "https://reqres.in/api/users?page=1";
		return get1_endpoint;
	}

	public static String get2_endpoint() {
		String get2_endpoint = "https://reqres.in/api/users?page=2";
		return get2_endpoint;
	}

}
