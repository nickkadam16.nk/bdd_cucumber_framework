package request_Repo;

import java.io.IOException;
import java.util.ArrayList;
import utility_Common_Methods.Excel_data_extractor;

public class Request_Repo {

	public static String post_request_repo() throws IOException {
		ArrayList<String> post_excel_data = Excel_data_extractor.excel_data_reader("ExcelData", "Post_API", "Post_Tc1");
		System.out.println("Fetching Post Payload from Excel file: " + post_excel_data);

		String name = post_excel_data.get(1);
		String job = post_excel_data.get(2);
		String post_requestBody = "{\r\n" + "    \"name\": \""+name+"\",\r\n" + "    \"job\": \""+job+"\"\r\n"+ "}";
		return post_requestBody;
	}

	public static String put_request_repo() throws IOException {
		ArrayList<String> put_excel_data = Excel_data_extractor.excel_data_reader("ExcelData", "Put_API", "Put_Tc4");
		System.out.println("Fetching Put Payload from Excel file: " + put_excel_data);

		String name = put_excel_data.get(1);
		String job = put_excel_data.get(2);
		String put_requestBody = "{\r\n" + "    \"name\": \""+name+"\",\r\n" + "    \"job\": \""+job+"\"\r\n"+ "}";
		return put_requestBody;
	}

}
