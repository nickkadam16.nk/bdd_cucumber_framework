package request_Repo;

public class DD_Request_Repo {

	public static String post_request_repo(String reqName, String reqJob) {
		String post_requestBody = "{\r\n" + "    \"name\": \"" + reqName + "\",\r\n" + "    \"job\": \"" + reqJob
				+ "\"\r\n" + "}";
		return post_requestBody;
	}

	public static String put_request_repo(String reqName, String reqJob) {
		String put_requestBody = "{\r\n" + "    \"name\": \"" + reqName + "\",\r\n" + "    \"job\": \"" + reqJob
				+ "\"\r\n" + "}";
		return put_requestBody;
	}

}
