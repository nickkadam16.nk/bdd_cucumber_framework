package fake_cucumber_runner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/fake_features", glue = { "fake_step_def" }, tags = "@fake_dd_post")
public class Fake_runner {

}
